import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';

declare const require: any;
const transactions = require('../transactions.json');

@Injectable()
export class MoneyAPI {
  transactions: Array<Transaction> = [];

  constructor() {

  }

  public fetchTransactionsFromFile() {
    this.transactions = transactions.data as Array<Transaction>;
  }

  public getTransactions(): Array<Transaction> {
    return this.transactions;
  }

  public makeTransaction(transaction: Transaction) {
    this.transactions.push(transaction);
  }
}


export interface Transaction {
  amount: number;
  categoryCode: string;
  merchant: string;
  merchantLogo: string;
  transactionDate: number;
  transactionType: string;
}



