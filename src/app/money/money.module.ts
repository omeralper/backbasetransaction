import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {MoneyRoutingModule} from './money.routing.module';
import {MoneyTransactionListComponent} from './money.transaction-list.component';
import {MoneyTransferComponent} from './money.transfer.component';
import {MoneyAPI} from '../APIs/MoneyAPI';
import {CustomFormsModule} from 'ng2-validation';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MoneyRoutingModule,
    CustomFormsModule
  ],
  declarations: [
    MoneyTransactionListComponent,
    MoneyTransferComponent
  ],
  providers: [MoneyAPI],
})
export class MoneyModule {
}
