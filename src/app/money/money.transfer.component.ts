import {Component, Inject, OnInit} from '@angular/core';
import {MoneyAPI, Transaction} from '../APIs/MoneyAPI';
import {Router} from '@angular/router';
import {CURRENT_USER, ICurrentUser} from '../app.current-user';

@Component({
  selector: 'money-transfer-list',
  templateUrl: './money.transfer.component.html',
})
export class MoneyTransferComponent implements OnInit {
  transaction: Transaction = <Transaction>{};

  constructor(public moneyAPI: MoneyAPI, public router: Router, @Inject(CURRENT_USER) public user: ICurrentUser) {

  }

  ngOnInit() {
  }

  submit() {
    $('#myModal').modal('hide');
    this.transaction.transactionDate = (new Date()).getTime();
    this.transaction.categoryCode = this.generateRandomColor();
    this.transaction.transactionType = 'Transaction';
    this.moneyAPI.makeTransaction(this.transaction);
    this.user.currentUserBalance -= this.transaction.amount;
    this.router.navigate(['/money/transactions']);
  }

  getMaxTransactionLimit() {
    return Math.min(this.user.currentUserBalance, 500);
  }

  generateRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
}
