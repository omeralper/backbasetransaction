import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MoneyTransactionListComponent} from './money.transaction-list.component';
import {MoneyTransferComponent} from './money.transfer.component';

@NgModule({
  imports: [
    RouterModule.forChild([
        {
          path: 'money',
          children: [
            {path: '', pathMatch: 'prefix', redirectTo: 'transactions'},
            {path: 'transactions', component: MoneyTransactionListComponent},
            {path: 'transfer', component: MoneyTransferComponent}
          ]
        },
      ]
    )
  ],
  exports: [
    RouterModule
  ]
})
export class MoneyRoutingModule {
}
