import {Component, OnInit} from '@angular/core';
import {MoneyAPI, Transaction} from '../APIs/MoneyAPI';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'money-transaction-list',
  templateUrl: './money.transaction-list.component.html',
})
export class MoneyTransactionListComponent implements OnInit {
  allTransactions: Array<Transaction> = [];
  filteredTransactions: Array<Transaction> = [];
  TransactionSortCriteria = TransactionSortCriteria;
  searchString: string;

  amountSorted = false;
  dateSorted = false;
  beneficiarySorted = false;

  constructor(private moneyAPI: MoneyAPI, public domSanitizer: DomSanitizer) {

  }

  ngOnInit() {
    this.allTransactions = this.moneyAPI.getTransactions();
    this.filteredTransactions = JSON.parse(JSON.stringify(this.allTransactions));
    this.sortTransactions(TransactionSortCriteria.DATE);
  }

  /**
   * https://angular.io/guide/pipes#appendix-no-filterpipe-or-orderbypipe
   * It is not good idea to write filter pipe, therefore filtering is handled at component
   */
  filterTransactions(searchString: string) {
    const lowerSearchString = searchString.toLowerCase();
    this.filteredTransactions =
      this.allTransactions.filter((transaction) => {
        return (transaction.amount + '').includes(lowerSearchString) ||
          transaction.merchant.toLocaleLowerCase().includes(lowerSearchString) ||
          transaction.transactionType.toLocaleLowerCase().includes(lowerSearchString) ||
          this.getLocaleDateString(transaction.transactionDate).toLocaleLowerCase().includes(lowerSearchString);
      });
  }

  sortTransactions(criteria: TransactionSortCriteria) {
    switch (criteria) {
      case TransactionSortCriteria.AMOUNT:
        this.filteredTransactions = this.filteredTransactions.sort((a, b) => {
          return this.amountSorted ? a.amount - b.amount : b.amount - a.amount;
        });
        this.amountSorted = !this.amountSorted;
        break;
      case TransactionSortCriteria.BENEFICIARY:
        this.filteredTransactions = this.filteredTransactions.sort((a, b) => {
          return this.beneficiarySorted ? a.merchant.toLocaleLowerCase() > b.merchant.toLocaleLowerCase() ? 1 : -1 :
            b.merchant.toLocaleLowerCase() > a.merchant.toLocaleLowerCase() ? 1 : -1;
        });
        this.beneficiarySorted = !this.beneficiarySorted;
        break;
      case TransactionSortCriteria.DATE:
        this.filteredTransactions = this.filteredTransactions.sort((a, b) => {
          if (this.dateSorted) {
            return new Date(a.transactionDate) > new Date(b.transactionDate) ? 1 : -1;
          } else {
            return new Date(b.transactionDate) > new Date(a.transactionDate) ? 1 : -1;
          }

        });
        this.dateSorted = !this.dateSorted;
        break;
    }
  }

  getLocaleDateString(dateNumber: number) {
    return (new Date(dateNumber)).toLocaleString('en-us', {month: 'short', day: 'numeric'});
  }
}

export enum TransactionSortCriteria {
  DATE,
  BENEFICIARY,
  AMOUNT
}

