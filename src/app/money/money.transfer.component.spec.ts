import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import 'rxjs/add/observable/of';
import * as $ from 'jquery';
import * as bootstrap from 'bootstrap';
import {RouterTestingModule} from '@angular/router/testing';
import {MoneyAPI} from '../APIs/MoneyAPI';
import {MoneyTransferComponent} from './money.transfer.component';
import {FormsModule} from '@angular/forms';
import {CURRENT_USER} from '../app.current-user';

describe('Money Transfer Component', () => {

  let comp: MoneyTransferComponent;
  let fixture: ComponentFixture<MoneyTransferComponent>;
  let de: DebugElement;
  let el: HTMLElement;
  // async beforeEach
  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule],
      declarations: [MoneyTransferComponent],
      providers: [{provide: MoneyAPI, useValue: moneyAPIStub},
        {provide: CURRENT_USER, useValue: {
          currentUserName: 'Free Checking(4692)',
          currentUserBalance: 5824.76
        }}]
    })
      .compileComponents();
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyTransferComponent);
    comp = fixture.componentInstance;
  });

  it('submit button should be passive ', () => {
    fixture.detectChanges();
    const submitButton: HTMLInputElement = fixture.debugElement.query(By.css('.submit-button')).nativeElement;
    expect(submitButton.disabled).toBe(false);
  });

  it('initial state of from account should be free checking $5824.76 ', () => {
    fixture.detectChanges();
    const field: HTMLInputElement = fixture.debugElement.query(By.css('#fromAccount')).nativeElement;
    expect(field.value).toEqual('Free Checking(4692) $5824.76');
  });

});

const moneyAPIStub = {
  makeTransaction: function () {

  }
};

