import {InjectionToken} from '@angular/core';

export let CURRENT_USER = new InjectionToken('app.config');

export interface ICurrentUser {
  currentUserName: string;
  currentUserBalance: number;
}

export const CurrentUserInfo: ICurrentUser = {
  currentUserName: 'Free Checking(4692)',
  currentUserBalance: 5824.76
};
