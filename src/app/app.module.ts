import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing.module';
import {MoneyModule} from './money/money.module';
import {MoneyAPI} from './APIs/MoneyAPI';
import {CURRENT_USER, CurrentUserInfo} from './app.current-user';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MoneyModule,
    AppRoutingModule
  ],
  providers: [MoneyAPI,
    {
      provide: APP_INITIALIZER,
      useFactory: (moneyApi: MoneyAPI) => function () {
        return moneyApi.fetchTransactionsFromFile();
      },
      deps: [MoneyAPI],
      multi: true
    },
    {provide: CURRENT_USER, useValue: CurrentUserInfo}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
