# BackbaseTransaction

This assignment is submitted by Omer Alper Ozkan to "backbase" as a job interview task. 

I have used angular2 because I've been using angular2 for 2 years and haven't used angular1 since then. 
I needed to remember angular1. That's why I had to use angular2. 
I hope using angular-cli wouldn't be problem for you. Angular-cli is officially used by angular team for long time 
to create a project from scratch.  

#  run the application:
 
npm i
npm start

the site will be published at localhost:4200

# test the application:

npm test


The application structured according to angular conventions. https://angular.io/guide/styleguide

I have money module (src/app/money) in addition to the main module. In the money module there are only 2 components:
transaction-list and transfer.

Transaction list component lists all transactions.
Transfer component is for money transfer page.

I tried to make the site mobile first, that is why screens are separated. When the money transfer is done, page redirects
to transactions list.  

I have only 1 API service that reads the transactions data from the file when app starts.  


Bundling, simplifying, uglifying are done by angular-cli. I could have done these things manually to show my skills but automatization 
is nice thing :) I'd prefer to focus on code rather than devOps things. And as you know, npm start runs ng serve, which is development 
server. It could have done like: ng build --aot --prod. I am leaving it as is, for simplicity. 

Thank you for your time. 
